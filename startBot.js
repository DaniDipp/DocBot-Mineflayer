const mineflayer = require('mineflayer');
const accountData = require('./accountData');
const vec3 = require('vec3');
const bot = mineflayer.createBot({
    host: accountData.host,
    port: accountData.port,
    username: accountData.username,
    password: accountData.password,
    verbose: accountData.verbose
});
const chat = require('./chatHandler');
const navConfig = require('./navigationConfig');
const inv = require('./inventory');

var location;

navConfig.init(bot);

bot.on('death', () => {
   if(bot.game.dimension == 'nether') location = 'nether';
})

bot.on('chat', (username, message) => {
    if (!message.startsWith(bot.username)){return;}
message = message.split(' ');
message.shift(); //Removing first element, bot.name
if(!chat.getFarming()){
    switch (message.shift()) {
        case 'come':
            chat.come(bot, username);
            break;
        case 'goto':
            chat.goto(bot, username, message);
            break;
        case 'stats':
            chat.stats(bot, username, message);
            break;
        case 'farmObsidian':
            chat.farmObsidian(bot);
            break;
        case 'getDimension':
            bot.chat('I\'m in the ' + bot.game.dimension);
            break;
        case 'inventory':
            inv.sayItems(bot);
            break;
        case 'toss':
            var name = message[0];
            var amount = parseInt(message[1]);
            if(typeof name == 'undefined' || isNaN(amount) || amount === 0){
                bot.chat('Syntax: toss [name] [amount]');
                return;
            }
            inv.tossItem(bot, name, amount);
            break;
        case 'stop':
            chat.stop(bot);
            break;
        case 'shutdown':
            bot.quit(bot);
            break;
        default:
            bot.chat('I don\'t understand.');
            break;
    }
} else {
    if(message.shift() == 'stop'){
        chat.stop(bot);
    }else {
        bot.chat('Shut up, I\'m farming!');
    }
}
})

//bot.on('death', () => {
//    bot.chat('RIP. Please teleport me to somebody while DaniDipp figures out how to make me respawn on my own.');
//})