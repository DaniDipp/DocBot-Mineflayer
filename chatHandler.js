const vec3 = require('vec3');

var farming = false; //Bot is currently engaged in a farming activity

function come(bot, username) {
    var target = bot.players[username].entity;
    try {
        bot.navigate.to(target.position);
    } catch (err){
        bot.chat('I can\'t find you, ' + username);
    }
}

function goto(bot, username, params) {
    if(params.length != 3){
        bot.chat('Please tell me the X, Y and Z coordinate.')
        return;
    }
    var x = parseFloat(params[0]);
    var y = parseFloat(params[1]);
    var z = parseFloat(params[2]);

    if(isNaN(x+y+z)) {
        bot.chat('These are not valid coordinates!');
        return;
    }
    var target = vec3(x, y, z);
    bot.navigate.to(target);
}

function stats(bot) {
    bot.chat('Health: ' + bot.health.toFixed(0)
    + ', Food: ' + bot.food.toFixed(0)
    + ', Saturation: ' + bot.foodSaturation.toFixed(0)
    + ', Levels: ' + bot.experience.level
    + ', Progress: ' + (bot.experience.progress*100).toFixed(0) + '%')
}

function farmObsidian(bot) {
    if (farming) {
        bot.chat('I\'m already farming. Use "stop" to stop');
    } else {
        farming = true;
        bot.chat('Started farming Obsidian.');
        continueFarmingObsidian(bot);
    }
}

function continueFarmingObsidian(bot) {
    timeout(function () {
        bot.chat('walking'); //TODO: Replace with goto
        if (farming) continueFarmingObsidian(bot);
        else bot.chat('Stopped farming Obsidian');
    });
}

function timeout(callback) {
    setTimeout(callback, 2000);
}

function stop(bot) {
    if(farming){
        bot.chat('Finishing last round...');
        farming = false;
    } else {
        bot.chat('I\'m not doing anything!');
    }
}

function getFarming() {
    return farming;
}

module.exports.come = come;
module.exports.goto = goto;
module.exports.stats = stats;
module.exports.farmObsidian = farmObsidian;
module.exports.stop = stop;
module.exports.getFarming = getFarming;