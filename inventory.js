function sayItems (bot, items = bot.inventory.items()) {
    const output = items.map(itemToString).join(', ');
    if (output) {
        bot.chat(output);
    } else {
        bot.chat('empty');
    }
}

function itemToString (item) {
    if (item) {
        return `${item.name} x ${item.count}`;
    } else {
        return '(nothing)';
    }
}

function tossItem (bot, name, amount) {
    const item = itemByName(bot, name);
    if (!item) {
        bot.chat(`I have no ${name}`);
    } else if (amount) {
        bot.toss(item.type, null, amount, checkIfTossed);
    } else {
        bot.tossStack(item, checkIfTossed);
    }

    function checkIfTossed (err) {
        if (err) {
            bot.chat(`unable to toss: ${err.message}`);
        } else if (amount) {
            bot.chat(`tossed ${amount} x ${name}`);
        } else {
            bot.chat(`tossed ${name}`);
        }
    }
}

function itemByName (bot, name) {
    return bot.inventory.items().filter(item => item.name === name)[0];
}

module.exports.sayItems = sayItems;
module.exports.tossItem = tossItem;