const navigatePlugin = require('mineflayer-navigate')('mineflayer');

function init(bot){
    navigatePlugin(bot);

    bot.navigate.blocksToAvoid[132] = true; // avoid tripwire
    bot.navigate.blocksToAvoid[59] = false; // ok to trample crops

    //Progress shoutouts
    bot.navigate.on('pathPartFound', function (path) {
        bot.chat("Going " + path.length + " meters in the general direction for now.");
    });
    bot.navigate.on('pathFound', function (path) {
        bot.chat("I can get there in " + path.length + " moves.");
    });
    bot.navigate.on('cannotFind', function (closestPath) {
        bot.chat("unable to find path. getting as close as possible");
        bot.navigate.walk(closestPath);
    });
    bot.navigate.on('arrived', function () {
        bot.chat("I have arrived");
    });
    bot.navigate.on('interrupted', function () {
        bot.chat("stopping");
    });
}

module.exports.init = init;